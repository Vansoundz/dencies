export class Orders {
    order = {}
    // items = []
    // customer = {}
    // shipping = ''
    // paymentMethod = ''
    // paymentStatus = ''
    // amount = 0
    constructor(i, a, c, s, m, st) {
        this.order.items = i
        this.order.amount = a
        this.order.customer = c
        this.order.shipping = s === null ? "CBD not specified" : s
        this.order.paymentMethod = m === null ? "cash" : m
        this.order.paymentStatus = st
        this.order.date = new Date().toISOString().slice(0, 10)
        this.order.time = new Date().toTimeString().slice(0, 5)
    }
}