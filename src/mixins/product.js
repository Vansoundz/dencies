export default {
    data() {
        return {
            // product: {}
        }
    },
    computed: {
        cartProducts() {
            // return this.$store.state.cart;
            let p = new Set();
            let prods = []
            this.$store.state.cart.forEach(prd => {
                let n = 0
                this.$store.state.cart.forEach(prod => {
                    if (prd.id === prod.id) {
                        n += 1
                    }
                })
                prd['quantity'] = n
                p.add(prd)
            })
            p.forEach(prd => {
                prods.push(prd)
            })

            return prods
        },
        total() {
            let t = 0;
            if (this.cartProducts.length > 0) {
                this.cartProducts.forEach(p => {
                    t += p.price * p.quantity;
                });
                return t;
            } else {
                return t;
            }
        },
        products() {
            return this.$store.state.products;
        },
        productKeys() {
            let n = 0;
            for (let num in this.product) {
                n++;
            }
            // console.log(n);
            return n;
        }
    },
    methods: {
        removeProduct(id) {
            this.$store.commit("removeProduct", id);
        },
        increment(id) {
            this.$store.commit('increment', id)
        }
    }
};