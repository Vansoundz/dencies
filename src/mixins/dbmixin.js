import {
    DB,
    STORE
} from '../firebase/init'

export default {
    data() {
        return {
            loading: true
        }
    },
    methods: {
        getProducts() {
            DB.collection("products")
                .get()
                .then(snapshot => {
                    let prods = [];
                    snapshot.forEach(doc => {
                        let ref = STORE.ref(`/images/${doc.data().img}`);
                        ref.getDownloadURL().then(url => {
                            let data = doc.data();
                            data.src = url;
                            data.id = doc.id;
                            prods.push(data);
                        });
                        // console.log(data.source);
                    });
                    this.$store.state.products = prods;
                }).then(() => {
                    this.loading = false
                });
        }
    },
}