import {
    bus
} from "../main";

export default {
    methods: {
        addToCart(product) {
            bus.$emit('added', product)
            this.$store.commit('addToCart', product)
        },

    }
}