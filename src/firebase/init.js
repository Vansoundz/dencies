import firebase from 'firebase'
import firestore from 'firebase/firestore'
import storage from 'firebase/storage'
import auth from 'firebase/auth'


var firebaseConfig = {
    apiKey: "AIzaSyBAxs29hRagmm6o8y2qqCLZM6dz5MFOCaY",
    authDomain: "dencies.firebaseapp.com",
    databaseURL: "https://dencies.firebaseio.com",
    projectId: "dencies",
    storageBucket: "dencies.appspot.com",
    messagingSenderId: "821891621250",
    appId: "1:821891621250:web:5dac04e5dddc4c5708ef9b",
    measurementId: "G-GK7HV387NW"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
export const DB = firebaseApp.firestore()
export const STORE = firebaseApp.storage()
export const AUTH = firebaseApp.auth()