import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
    cart: [],
    categories: [
      'trending',
      'sneakers',
      'men',
      'women',
      'kids'
    ]
  },
  mutations: {
    addToCart(state, product) {
      state.cart.push(product)
    },
    resetCart(state) {
      state.cart = []
    },
    removeProduct(state, id) {
      let n = 0
      state.cart = state.cart.filter(prd => {
        if (prd.id === id) {
          n += 1
          if (n > 1) {
            return true
          } else {
            return false
          }
        }
        return prd.id !== id
      })
    },
    increment(state, id) {
      let target = null
      state.cart.forEach(prd => {
        if (prd.id === id) {
          target = prd
          return target
        }
      })
      state.cart.push(target)
    },
    suggestions(state, payload) {
      let suggestions = []
      state.products.forEach(product => {
        if (product.category == payload) {
          suggestions.push(product)
        }
      })
      return suggestions
    }
  },
  getters: {
    kids(state) {
      let prods = []
      state.products.forEach(prd => {
        if (prd.category === 'kids') {
          prods.push(prd)
        }
      })
      return prods
    },
    sneakers(state) {
      let prods = []
      state.products.forEach(prd => {
        if (prd.category === 'sneakers' || prd.category === 'unisex') {
          prods.push(prd)
        }
      })
      return prods
    },
    men(state) {
      let prods = []
      state.products.forEach(prd => {
        if (prd.category === 'men' || prd.category == 'sneakers') {
          prods.push(prd)
        }
      })
      return prods
    },
    women(state) {
      let prods = []
      state.products.forEach(prd => {
        if (prd.category === 'women' || prd.category == 'sneakers') {
          prods.push(prd)
        }
      })
      return prods
    },
    trending(state) {
      let prods = []
      state.products.forEach(prd => {
        if (prd.category === 'trending') {
          prods.push(prd)
        }
      })
      return prods
    },
  },
  actions: {

  }
})